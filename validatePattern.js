// validatePattern.js

// MIT License Copyright (c) 2019 OSFDA
//
// Permission is hereby granted, free of charge,
// to any person obtaining a copy of this software
// and associated documentation files (the "Software"),
// to deal in the Software without restriction,
// including without limitation the rights to
// use, copy, modify, merge, publish, distribute,
// sublicense, and/or sell copies of the Software,
// and to permit persons to whom the Software is
// furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice
// shall be included in all copies or substantial portions
// of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY
// OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
// ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

const FAIL_SAFE=false,
      FAIL_OPEN=true,
      // Set FAIL to FAIL_OPEN, if your interface validation need not be rigorous;
      // with thorough testing it should be evident whether it's working...
      FAIL=FAIL_SAFE;

const PASTE_DEFAULT_OPTIONS={trim: true};

function rightString(text, length)
// negative string offsets will be coerced to zero in JavaScript...
{return text.substring(text.length-length);
}

var BEEP=undefined; // we'll do a "lazy load"...

function beep()
{
 if (BEEP===undefined)
    BEEP=document.getElementById('beep'); // <audio id="beep" src="beep.mp3"></audio>

 if (BEEP!==null)
    BEEP.play ();
} // function beep()

function clipboard(event, options)
{var text=null;

 if (options===undefined)
    options=PASTE_DEFAULT_OPTIONS;

 if (window.clipboardData&&window.clipboardData.getData)
    text=window.clipboardData.getData('Text');

 if (event.clipboardData&&event.clipboardData.getData)
    text=event.clipboardData.getData('text/plain');

 if (text!==null&&
     'trim' in options&&
     options.trim
    )
    text=text.trim();

 return text;
} // function clipboard(event, options)

//const PRINTABLE_CHAR=/[\u0020-\u007e\u00a0-\u00ff]/;

const BACKSPACE_KEY=8,
      DELETE_KEY=46;

var compiled_regexes={};

function regexp(pattern)
{
 if (pattern in compiled_regexes)
    return compiled_regexes[pattern];

 let compiled_regexp=new RegExp(pattern);

 compiled_regexes[pattern]=compiled_regexp;
 return compiled_regexp;
} // function regexp(pattern)

var templates={};

const BRACKETED=/^\[.*\]$/,
      SINGLE_QUOTE_PATTERN=/'/g;

function parsedTemplates(template)
{var parsed;

 function templateErr ()
 {console.error ('validatePattern: bad data-entry-templates attribute value; '+
                 'expected an array of strings, or a single template string...'
                );
  console.error (`Example: data-entry-template="['abc', 'def']"`);
  console.error (`(or: data-entry-template='["abc", "def"]'  )`);
 } // function templateErr ()

 if (template in templates)
    return templates[template];

 let value=template.trim();
 if (BRACKETED.test(value))
    {if (value.indexOf("'")>-0)
        value=value.replace(SINGLE_QUOTE_PATTERN, '"');

     try {parsed=JSON.parse(value);
         }
     catch (err)
           {templateErr ();
            return null;
           } // catch...
     if (!Array.isArray(parsed))
        {templateErr ();
         return null;
        } // !Array.isArray(parsed)
    } // if (BRACKETED.test(value))

 else parsed=[value];

 for (let index=0; index<parsed.length; index++)
     if (typeof(parsed[index])!=='string')
        {templateErr ();
         return null;
        } // typeof(parsed[index])!=='string'

 templates[template]=parsed;
 return parsed;
} // function parsedTemplates(template)

function validatePattern(event)
{var entryText='', remainder;

 event=event||window.event;
 if (event.target.tagName.toLowerCase()!=='input')
    {console.error ('validatePattern: only meant for "input" elements; '+
                    'used on "'+event.target.tagName+'"...'
                   );
     return FAIL;
    } // event.target.tagName.toLowerCase()!=='input'

 let input=event.target;
 if (input.type.toLowerCase()!=='text')
    {console.error ('validatePattern: only meant for "input(text)" elements; '+
                    'used on input type="'+input.type+'"...'
                   );
     return FAIL;
    } // input.type.toLowerCase()!=='text'

 if ((!('pattern' in input))||
     input.pattern.length<=0
    )
    {console.error ('validatePattern: missing pattern attribute on input element...');
     return FAIL;
    } // (!('pattern' in input))||input.pattern.length<=0

 if ((!('entryTemplates' in input.dataset))||
     input.dataset.entryTemplates.length<=0
    )
    {console.error ('validatePattern: missing data-pattern-template attribute on input element...');
     return FAIL;
    } // (!('patternTemplate' in input.dataset))||input.dataset.entryTemplates.length<=0

 let prefix=input.value.substring(0, input.selectionStart),
     suffix=input.value.substring(input.selectionEnd);

 switch (event.type)
        {case 'keydown': // only intercept Backspace, Delete key...
              switch (event.keyCode)
                     {case BACKSPACE_KEY:
                           prefix=prefix.substring(0, prefix.length-1);
                           break; // BACKSPACE_KEY

                      case DELETE_KEY:
                           suffix=suffix.substring(1);
                           break; // DELETE_KEY

                      default: if (event.key.length!==1)
                                  return true; // allow...

                               entryText=event.key;
                     } // switch (event.keyCode)
              break; // keydown

         case 'paste':
              entryText=clipboard(event);
              break; // paste

         default: console.error ('validatePattern; unhandled event type: '+
                                 event.type
                                );
                  return true; // allow...
        } // switch (event.type)

 let dataEntryTemplates=parsedTemplates(input.dataset.entryTemplates);
 if (dataEntryTemplates===null)
    return FAIL;

 for (let index=0; index<dataEntryTemplates.length; index++)
     {let template=dataEntryTemplates[index],
          newText=entryText;

      if ('maxLength' in input&&
          input.maxLength>0
         )
         {template=template.substring(0, input.maxLength);
          remainder=prefix.length+suffix.length;

          newText=newText.substring(0, Math.min(input.maxLength-remainder,
                                                newText.length
                                               )
                                   );
         } // 'maxLength' in input&&input.maxLength>0

      let after=prefix+newText+suffix;

      // A JS interpreter I was using could NOT handle substitution of the remainder expression
      // into the rightString function -go figure!
      // Used an intermediate variable...
      remainder=template.length-after.length;
      if (regexp(input.pattern).test(after+rightString(template, remainder)))
         return true; // valid...
     } // for (let index=0; index<dataEntryTemplates.length; index++)

 beep ();
 return false; // invalid...
} // function validatePattern(event)
