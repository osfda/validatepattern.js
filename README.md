# validatePattern.js

A dependency-free validation function you can hook to an HTML [text] input element that performs keystroke-by-keystroke field validation **based on the standard HTML5 "pattern" attribute.**

[HTML5 ONLY...]

**How to use:**

Let's say you wanted to validate a bitcoin address entry (note that this will just be a speedy client-side preliminary validation, preventing entries that are not remotely possible; server-side RPC/GraphQL calls to a blockchain could validate it more rigorously after being posted...)

```
<input id="forward_address"
       name="forward_address"
       type="text"
       maxlength="90"
       pattern="^(bc(0([ac-hj-np-z02-9]{39}|[ac-hj-np-z02-9]{59})|1[ac-hj-np-z02-9]{8,87})|[13][a-km-zA-HJ-NP-Z1-9]{25,34}$"
       data-entry-templates="['bc099999999999999999999999999999999999999999999999999999999999','bc1999999999999999999999999999999999999999999999999999999999999999999999999999999999999999','19999999999999999999999999999999999']"
       onkeydown="return validatePattern(event)"
       onpaste="return validatePattern(event)"
       required
/>
```

Note that the data-entry-template attribute furnishes placeholder characters for temporary validation purposes before a user has had a chance to complete the entry. This way a user's time is not wasted entering -or pasting- characters that will later be inevitably rejected when they submit the form (and their mind is on other matters...)

For feedback, use the Contact button at https://bitmodeler.com